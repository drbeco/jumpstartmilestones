-- control.lua JumpStartMilestones
-- by Dr. Beco
-- 2023-12-21

remote.add_interface("JumpStart Base", {
    milestones_preset_addons = function()
        return {
            ["JumpStart Base"] = {
                required_mods = {},
                milestones = {
                    {type="group", name="Jumpstart Base Itens"},
                    {type="item", name="stone-brick", quantity=1},
                    {type="item", name="stone-brick", quantity=1000},
                    {type="item", name="iron-plate", quantity=1},
                    {type="item", name="iron-plate", quantity=1000},
                    {type="item", name="copper-plate", quantity=1},
                    {type="item", name="copper-plate", quantity=1000},
                    {type="item", name="steel-plate", quantity=1},
                    {type="item", name="steel-plate", quantity=1000},
                    {type="item", name="grenade", quantity=1},
                    {type="item", name="grenade", quantity=1000},
                    {type="item", name="underground-belt", quantity=1},
                    {type="item", name="underground-belt", quantity=1000},
                    {type="item", name="transport-belt", quantity=1},
                    {type="item", name="transport-belt", quantity=1000},
                    {type="item", name="steel-furnace", quantity=1},
                    {type="item", name="steel-furnace", quantity=100},
                    {type="item", name="engine-unit", quantity=1},
                    {type="item", name="engine-unit", quantity=100},
                    {type="item", name="pipe", quantity=1},
                    {type="item", name="pipe", quantity=1000},
                    {type="item", name="small-lamp", quantity=1},
                    {type="item", name="small-lamp", quantity=100},
                    {type="item", name="iron-gear-wheel", quantity=1},
                    {type="item", name="iron-gear-wheel", quantity=1000},
                    {type="item", name="electronic-circuit", quantity=1},
                    {type="item", name="electronic-circuit", quantity=1000},
                    {type="item", name="copper-cable", quantity=1},
                    {type="item", name="copper-cable", quantity=1000},
                    {type="item", name="long-handed-inserter", quantity=1},
                    {type="item", name="long-handed-inserter", quantity=50},
                    {type="item", name="inserter", quantity=1},
                    {type="item", name="inserter", quantity=100},
                    {type="item", name="fast-inserter", quantity=1},
                    {type="item", name="fast-inserter", quantity=50},
                    {type="item", name="splitter", quantity=1},
                    {type="item", name="splitter", quantity=50},
                    {type="item", name="electric-mining-drill", quantity=1},
                    {type="item", name="electric-mining-drill", quantity=50},
                    {type="item", name="firearm-magazine", quantity=1},
                    {type="item", name="firearm-magazine", quantity=1000},
                    {type="item", name="gun-turret", quantity=1},
                    {type="item", name="gun-turret", quantity=50},
                    {type="item", name="assembling-machine-2", quantity=1},
                    {type="item", name="assembling-machine-2", quantity=50},
                    {type="item", name="big-electric-pole", quantity=1},
                    {type="item", name="big-electric-pole", quantity=10},
                    {type="item", name="medium-electric-pole", quantity=1},
                    {type="item", name="medium-electric-pole", quantity=10},
                    {type="item", name="small-electric-pole", quantity=1},
                    {type="item", name="small-electric-pole", quantity=100},
                    {type="item", name="iron-stick", quantity=1},
                    {type="item", name="iron-stick", quantity=50},
                    {type="item", name="automation-science-pack", quantity=1},
                    {type="item", name="automation-science-pack", quantity=100},
                    {type="item", name="logistic-science-pack", quantity=1},
                    {type="item", name="logistic-science-pack", quantity=100},

                    {type="group", name="Jumpstart Base Infra"},
                    {type="item", name="offshore-pump",quantity=1},
                    {type="item", name="steam-engine", quantity=1},
                    {type="item", name="steam-engine", quantity=24},
                    {type="item", name="boiler", quantity=1},
                    {type="item", name="boiler", quantity=12},
                    {type="item", name="lab", quantity=1},
                    {type="item", name="lab", quantity=16},
                    {type="item", name="stone-furnace", quantity=1},
                    {type="item", name="stone-furnace", quantity=100},
                    {type="item", name="assembling-machine-1", quantity=1},
                    {type="item", name="assembling-machine-1", quantity=45}
                }
            }
        }
    end
})

