# Make for JumpStartMilestones Factorio Module
# Copyright (C) 2023 by Ruben Carlo Benante
# Contact: rcb@beco.cc
# Date: 2023-12-21

VERSION:=$(shell cat info.json | grep \"version\": | cut -d'"' -f4)

zip:
	echo "Creating JumpStart Base Milestones mod Version $(VERSION)"
	if [ -f JumpStartMilestones_$(VERSION).zip ] ; then echo removing old zip JumpStartMilestones_$(VERSION).zip ; rm -f JumpStartMilestones_$(VERSION).zip ; fi
	cd .. && zip -r JumpStartMilestones_$(VERSION).zip JumpStartMilestones -x@JumpStartMilestones/excludefromzip.lst && mv JumpStartMilestones_$(VERSION).zip JumpStartMilestones/

install:
	echo Removing old version from ~/.config/factorio/mods
	rm -f ~/.config/factorio/mods/JumpStartMilestones_*.zip
	echo Copying JumpStartMilestones_$(VERSION).zip to ~/.config/factorio/mods
	cp JumpStartMilestones_$(VERSION).zip ~/.config/factorio/mods

