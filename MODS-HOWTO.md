# Intro to Factorio MODS

## References

### General

* https://wiki.factorio.com/Tutorial:Modding_tutorial/Gangsir
* https://lua-api.factorio.com/latest/
* https://wiki.factorio.com/Modding#Creating_mods

### Classes, events and prototypes

* https://lua-api.factorio.com/latest/classes.html
* https://lua-api.factorio.com/latest/events.html
* https://lua-api.factorio.com/latest/index-prototype.html

### Stages 

* https://wiki.factorio.com/Tutorial:Mod_settings

## Teminology

* Prototype: describes an abstract instance of an entity, item or recipe
* Event: triggered internally, may be connected to a function of a module
* Item: things that goes in inventories

## Load order

Mods are load in the following order:

1. Dependency, and then
2. alphabetically

Dependencies can be: required, optional and restrictive.

* Required: prevents module from loading if the requirements are not met
* Optional: don't prevent load, but if found will give modules more options when programmed
* Restrictive: prevents a module from loading if other module are loaded


## Stages

### Settings stage

Load every module to present them in the mod settings GUI.

It reads settings.lua for all modules, and then settings-updates.lua for each module; finally settings-final-fixes.lua is called.

This stage doesn't have access to prototype or runtime data.

### Data stage

Only used to declare prototypes

File for each module: data.lua
Also, after that: data-updates.lua and data-final-fixes.lua

### Migrations

Scripts that help to fix mod updates when name change or technology unlocks, or other important things that may break automatic updates

### Runtime stage

This is dealt by control.lua file

Use event handlers to access tables during game.

## Summary of files and directories for the module


* info.json - this is the only mandatory file, identifies the mod and author
* changelog.txt - version history
* thumbnail.png - the icon
* settings.lua - for configurable settings (settings-updates.lua, settings-final-fixes.lua)
* data.lua - for new entities (data-updates.lua, data-final-fixes.lua)
* control.lua - for in-game effects
* locale/en/module-name.cfg - locale for more languages
* migrations/data-migrations.json - identify name changes in updates

### sub-folders:

* locale - for translations
* scenarios - custom scenarios
* campaigns - custom campaigns
* tutorials - custom tutorials
* migrations - for migrations files



### Mod settings

* https://wiki.factorio.com/Tutorial:Mod_settings

