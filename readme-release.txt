How to release a new version:

Scenario:
- You completely released version vA.A.A with tag at gitlab and in the mod website
- You still don't know the number of the next release
- You are above version vA.A.A now


1. edit the necessary source code files
2. commit as you like
3. generate zip -- make zip
4. install -- make install
5. test
6. go back to step 1
---

- You are now satisfied with the code
- You decided that this modification deserves version number vB.B.B

7. edit info.json and set version vB.B.B
8. edit changelog.txt and describe the changes
9. commit as you like
10. generate zip -- make zip
11. install -- make install
12. check the in-game changelog

---


- You are satisfied with the changelog

13. tag the version number
14. push and push tags
15. get the zip file to the mods website


